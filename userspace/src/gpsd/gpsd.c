/*
 * gpsd.c
 *
 * Columbia University
 * COMS W4118 Fall 2012
 * Homework 6
 *
 */
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <linux/unistd.h>

#include "gpsd.h"

/* after daemon-izing, STDOUT/STDERR will show up in this file */
/* use this log file to report updates on gpsd after daemonizing */
#define LOG_FILE "/data/misc/gpsd.log"

/* reading GPS coordinates (seconds) */
#define GPSD_FIX_FREQ  1

static int received_signal;

static void sig_handler (int signal)
{
	received_signal = 1;
	infop("received signal\n");
}

static int valid_location(struct gps_location *location)
{
	return location->longitude >= -180. && location->longitude <= 180. &&
		location->latitude >= -90. && location->latitude <= 90.;
}

int main(int argc, char **argv)
{
	/* daemonize */
	struct sigaction sigact = {
		.sa_handler = &sig_handler
	};

	if (sigaction(SIGINT, &sigact, NULL) ||
		sigaction(SIGQUIT, &sigact, NULL) ||
		sigaction(SIGTERM, &sigact, NULL)) {
		errp("gpsd: error installing signal handlers; exiting\n");
		return EXIT_FAILURE;
	}

	close(0);
	close(1);
	close(2);

	int ret = daemon(0, 1);
	if (ret) {
		errp("gpsd: failed to daemonize; exiting\n");
		return EXIT_FAILURE;
	}

	/* redirect standard streams */
	if (open("/dev/null", O_RDONLY) != 0)
		return EXIT_FAILURE;

	if (open(LOG_FILE, O_CREAT | O_WRONLY | O_SYNC) != 1)
		return EXIT_FAILURE;

	if (dup(1) != 2)
		return EXIT_FAILURE;

	while (!received_signal) {
		/* Q: Do we need some locking here? */

		/* read GPS values stored in GPS_LOCATION_FILE */

		FILE *file = fopen(GPS_LOCATION_FILE, "r");
		if (file == NULL) {
			errp("gpsd: failed open location file\n");
			continue;
		}

		double latitude, longitude;
		float accuracy;

		if (3 != fscanf(file, "%lf\n%lf\n%f\n", &latitude, &longitude,
			&accuracy)) {
			errp("gpsd: error reading gps coords\n");

			if (fclose(file))
				errp("gpsd: error closing file\n");

			continue;
		}

		if (fclose(file))
			errp("gpsd: error closing file\n");

		/* send GPS values to kernel using system call */
		struct gps_location location;
		location.latitude = latitude;
		location.longitude = longitude;
		location.accuracy = accuracy;

		if (!valid_location(&location)) {
			errp("gpsd: invalid location\n");
			continue;
		}

		if (syscall(__NR_set_gps_location, &location))
			errp("gpsd: error calling system call\n");

		infop("gpsd: %g %g %g\n", latitude, longitude, accuracy);

		fflush(stdout);
		fflush(stderr);

		/* sleep for one second */
		sleep(GPSD_FIX_FREQ);
	}

	return EXIT_SUCCESS;
}

