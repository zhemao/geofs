#ifndef _LINUX_GPS_H
#define _LINUX_GPS_H

#define set_gps_location(loc) syscall(376, loc)

struct gps_location {
	double latitude;
	double longitude;
	float accuracy;
};

#endif
