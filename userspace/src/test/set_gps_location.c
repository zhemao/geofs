#include "gps.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	struct gps_location loc;

	if (argc < 4) {
		fprintf(stderr, "Usage: %s lat lon accuracy\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	loc.latitude = atof(argv[1]);
	loc.longitude = atof(argv[2]);
	loc.accuracy = atof(argv[3]);

	if (set_gps_location(&loc)) {
		perror("set_gps_location");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
