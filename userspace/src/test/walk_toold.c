#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define WALK_TOOL "/data/misc/walk_tool"
#define TMP_FILE "/data/misc/hmwk6/tmpfile.txt"
#define LOG_FILE "/data/misc/hmwk6/walklog"

int main(void)
{
	if (daemon(0, 0)) {
		perror("daemon");
		return EXIT_FAILURE;
	}

	execl(WALK_TOOL, WALK_TOOL, TMP_FILE, LOG_FILE, 0);

	return EXIT_FAILURE;
}
