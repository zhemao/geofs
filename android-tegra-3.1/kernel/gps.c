#include <linux/namei.h>
#include <linux/gps.h>
#include <linux/syscalls.h>
#include <linux/uaccess.h>

struct gps_location gps_current;
struct timespec gps_current_timestamp;
spinlock_t gps_current_lock;

/**
 * sys_set_gps_location: sets the GPS location and timestamp.
 * @location: the new device GPS location.
 */
SYSCALL_DEFINE1(set_gps_location, struct gps_location __user *, location)
{
	struct gps_location tmp_loc;

	if (current_uid() != 0 && current_euid() != 0)
		return -EACCES;

	if (location == NULL)
		return -EINVAL;

	if (copy_from_user(&tmp_loc, location, sizeof(tmp_loc)))
		return -EFAULT;

	spin_lock(&gps_current_lock);
	gps_current = tmp_loc;
	getnstimeofday(&gps_current_timestamp);
	spin_unlock(&gps_current_lock);

	return 0;
}

static int inode_readable(struct inode *inode)
{
	if (current_uid() == 0)
		return 1;

	if (current_uid() == inode->i_uid && inode->i_mode & S_IRUSR)
		return 1;

	if (current_gid() == inode->i_gid && inode->i_mode & S_IRGRP)
		return 1;

	if (inode->i_mode & S_IROTH)
		return 1;

	return 0;
}

SYSCALL_DEFINE2(get_gps_location, const char __user *, pathname,
		struct gps_location __user *, location)
{
	struct path p;
	struct inode *i;
	struct gps_location tmp_loc;
	u32 coord_age;

	if (location == NULL)
		return -EINVAL;

	if (!access_ok(VERIFY_WRITE, location, sizeof(struct gps_location)))
		return -EFAULT;

	if (user_path(pathname, &p))
		return -ENOENT;

	i = p.dentry->d_inode;

	spin_lock(&i->i_lock);

	if (!i->i_op || !i->i_op->get_gps_location) {
		spin_unlock(&i->i_lock);
		return -ENOENT;
	}

	if (!inode_readable(i)) {
		spin_unlock(&i->i_lock);
		pr_info("inode not readable by current user");
		return -EACCES;
	}

	coord_age = i->i_op->get_gps_location(i, &tmp_loc);
	spin_unlock(&i->i_lock);

	if (copy_to_user(location, &tmp_loc, sizeof(tmp_loc)))
		return -EFAULT;

	return coord_age;
}

static int __init gps_init(void)
{
	spin_lock_init(&gps_current_lock);
	gps_current_timestamp.tv_sec = 0;
	gps_current_timestamp.tv_nsec = 0;
	return 0;
}

module_init(gps_init);
