#ifndef _LINUX_GPS_H
#define _LINUX_GPS_H

#include <linux/spinlock.h>
#include <linux/time.h>

struct gps_location {
	double latitude;
	double longitude;
	float accuracy;
};

extern struct gps_location gps_current;
extern struct timespec gps_current_timestamp;
extern spinlock_t gps_current_lock;

#endif
